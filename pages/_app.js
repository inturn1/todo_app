import MenuBar from '../components/menuBar'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <MenuBar>
        <Component {...pageProps} />
      </MenuBar>
      
    </>
  )
}

export default MyApp
