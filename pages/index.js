import styles from '../styles/Home.module.css'
import { Layout } from 'antd';
import React, { useEffect, useState } from 'react';
export default function Home() {


  //to get the data from localstorage
  useEffect(() => {
    const listData = JSON.parse(localStorage.getItem('lists'));
    if (listData) {
      setTodoList(listData);
    }
  }, []);

  const [userInput, setUserInput] = useState('')
  const [todoList, setTodoList] = useState([]);

  //delete todo in todoList
  function handleDelete(t) {
    setTodoList(
      todoList.filter((todo) => {
        localStorage.removeItem("lists")
        return todo != t;
      })
    );
  }

  // const handleDelete = (todo) => {
  //   const updatedArr = todoList.filter
  //     (todoItem => todoList.indexOf(todoItem) != todoList.indexOf(todo))

  //   setTodoList(updatedArr)
  // }

  //press enter and add todo in todoList
  const pressEnterSubmit = (event) => {
    if (event.key === "Enter" && !todoList.includes(userInput)) {
      event.preventDefault()
      let newTodo = todoList
      newTodo.push(userInput)
      setTodoList(newTodo)
      setUserInput('')
      localStorage.setItem('lists', JSON.stringify(todoList));
    }
  }

  const handleChange = (e) => {
    e.preventDefault();
    setUserInput(e.target.value)
  }



  //add data to localStorage
  useEffect(() => {
    if (todoList.length != 0) {
      localStorage.setItem('lists', JSON.stringify(todoList))
    }
  }, [todoList]);

  return (
    <div className={styles.main}>
      {/* left side */}
      <form>
        <p className={styles.p} >todos</p><br />
        <input className={styles.input} type="text"
          required
          value={userInput}
          placeholder='What needs to be done?'
          onChange={handleChange}
          onKeyDown={pressEnterSubmit}
        />
      </form>
      <div>
        <ul>
          {todoList.length >= 1 ? todoList.map((todo, idx) => {
            return <li key={idx} > {todo} <button onClick={(e) => {
              e.preventDefault()
              handleDelete(todo)
            }}>Delete</button></li>
          })
            : ""
          }
        </ul>
      </div>
    </div>
  )
}
