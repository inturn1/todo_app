import Link from "next/Link";
import styles from '../styles/Home.module.css'


export default function MenuBar({ children }) {
return (
  <div className='flex min-h-screen'>
      <aside className='bg-gray-200 w-full md:w-60'>
        <div>
          <p className="text-3xl">AngularJS</p>
          <p>Example</p>
        </div>
      </aside>
        <main className={styles.main}>{children}</main>
  </div>
  )
}
